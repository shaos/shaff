; SHAFF0 block depacker for 8080/8085 written by Shaos on 27-JAN-2024
; See http://nedoPC.org/forum/ for more info about SHAFF packer
; This code is PUBLIC DOMAIN - use it on your own RISK!

; This depacker is doing decoding of a single data block with size <=16KB
; No SHAFF header is expected - it's already known that it's SHAFF0 format:
; 1st byte sets a Key to use further instead of #FF (but usually it's #FF)
; then any byte other than Key goes directly to output
; #FF 00000000 - single byte #FF, otherwise
; #FF 0xxxxxxx LENGTH - distance 1..127 and LENGTH (see below)
; #FF 10xxxxxx LENGTH - distance 128..190 and LENGTH (see below), but
; #FF 10111111 LENGTH - reuses previous long distance -191 or longer
; #FF 11xxxxxx xxxxxxxx LENGTH - directly encoded long negative distance, but
; #FF 11000000 00000000 - end of block (with no length after)
; where LENGTH encoded by 1 or 2 bytes:
; 1xxxxxxx - for 4..131
; 01xxxxxx - for 132..195
; 00xxxxxx xxxxxxxx - direcly encoded length for up to 16383

; Size of assembled code is 137 bytes
; HL - address of packed data (data starts with a key byte)
; DE - pointer to the buffer where depacked data should appear (16KB max)
; Subroutine uses 2 bytes of memory as variable (DSH0D)

DESHAFF0:
	mov	b,m	; B is a Key byte (#FF in normal case)
	inx	h
DSH0L:  mov	a,m	; +7=7  ; main loop
	inx	h	; +5=12
	cmp	b	; +4=16 ; compare with a Key
	jz	DSH0FF	; +10=26
DSH0LL:	stax	d	; +7=33 ; it was not a Key so decode byte as is
	inx	d	; +5=38
	jmp	DSH0L	; +10=48 ; 1 byte of unpackable data takes 48 clock cycles
DSH0FF:	mov	a,m	; read byte after Key
	inx	h
	ora	a	; check for 0
	jnz	DSH0F1
	mov	a,b	; it was 0 after a Key so decode it as Key value
	jmp	DSH0LL	; go back to the loop
DSH0F1:	mov	c,a	; it was not 0 after a Key so store it and
	ani	0C0H	; check if it's 1-byte distance
	cpi	0C0H	; by comparing 2 most significant bits with 11
	jz	DSH0F2	; go to handle 2-byte case
	; 1-byte distance
	mov	a,c	; now check if distance is 191 (special case)
	cpi	0BFH
	jz	DSH0F3	; go to special case handler
	push	b	; temporarily push Key to stack
	xra	a	; clear A and flagC
;	mov	b,a	; here BC is a distance back (1..190)
;	mov	a,b	; invert B
	cma
	mov	b,a
	mov	a,c	; invert C
	cma
	mov	c,a
	inx	b	; make it two's complement negative number
	jmp	DSH0LN	; go to read length
	; 2-byte distance
DSH0F2:	push	b	; temporary push Key to stack
	mov	b,c	; it should be 2-byte distance, so this is higher byte
	mov	c,m	; read lower byte of the distance
	inx	h
	; now BC is a negative distance for 2-byte case - save it
	mov	a,c
	sta	DSH0D
	mov	a,b
	sta	DSH0DH
	; check for end of the block
	mov	a,b
	cpi	0C0H	; compare higher byte with #C0
	jnz	DSH0LN	; go to read length
	mov	a,c
	ori	0	; compare lower byte with #00
	jnz	DSH0LN	; go to read length  
	pop	b	; balance the stack before return
	ret
DSH0F3:	push	b	; temporary push Key to stack
	; retrieve stored distance
	lda	DSH0D	
	mov	c,a
	lda	DSH0DH
	mov	b,a	
	; now BC is a negative distance for special case
DSH0LN:	mov	a,m	; read length code from stream
	add	a	; check if bit 7 of length code was 1
	jnc	DSH0L1	; if not go to next step
	; 1xxxxxxx - Length 4..131 (0x80->4, 0x81->5 ... 0xFF->131)
	mov	a,m	; read length code from stream again
	sui	7CH	; convert code to actual length
DSH0L0:	inx	h
	push	d	; temporarily push DE to stack
	mvi	d,0	; higher byte of the length is 0
	mov	e,a	; lower byte of the length is taken from A
	jmp	DSH0L3
DSH0L1:	add	a	; check if bit 6 of length code was 1
	jnc	DSH0L2	; if not go to next step
	; 01xxxxxx - Length 132..195 (0x40->132, 0x41->133 ... 0x7F->195)
	mov	a,m	; read length code from stream again
	adi	44H	; convert code to actual length
	jmp	DSH0L0
DSH0L2:	push	d	; temporarily push DE to stack
	; 00xxxxxx xxxxxxxx - Length up to 16383
	mov	d,m	; read higher byte of the length from stream again
	inx	h
	mov	e,m	; read lower byte of the length from stream
	inx	h
DSH0L3:	xthl		; exchange top of the stack (stored DE) with HL
	push	d	; push length of the data to stack
	mov	d,h
	mov	e,l	; now DE is current destination address
	dad	b	; now HL is address of the reference
	pop	b	; now BC is length of the data to copy
	; perform copying of the data (as if it's LDIR)
DSH0L4:	mov	a,m
	stax	d
	inx	h
	inx	d
	dcx	b
	mov	a,c
	ora	b
	jnz	DSH0L4
	pop	h	; restore old source
	pop	b	; restore a Key
	jmp	DSH0L	; go back to main loop
DSH0D:	db	0	; lower byte of last used long distance
DSH0DH:	db	0	; higher byte of last used long distance
