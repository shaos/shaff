; SHAFF0 block depacker for Z80/Z180 written by Shaos in March 2021
; This code is PUBLIC DOMAIN - use it as you want, but on your own RISK!
; See http://nedoPC.org/forum/ for more info about SHAFF packer
; Latest sources: https://gitlab.com/shaos/shaff

; 28-MAR-2021 - initial version with relocatable depacker only
; 29-MAR-2021 - conditional compilation for non-relocatable depacker added
; 30-MAR-2021 - conditional compilation for non-relocatable turbo depacker

TURBO	equ	1 ; to make non-relocatable faster code
TURBO2	equ	0 ; to switch to sequence of LDI instead of LDIR for sizes > 12

; How to build: zmac -m deshaff0.asm (after setting appropriate values above)
; Relocatable code is 104 bytes long with speed <=49t/byte (2.33*LDIR)
; Non-relocatable full turbo code is 398 bytes long with speed varying
; from 34.95t/byte (1.66*LDIR) for uncompressable data
; to 39.06t/byte (1.86*LDIR) for real life Z80 code block
; so turbo depacker might be about 29% faster than relocatable one
; if you disable TURBO2 the size goes down to 241 bytes with ~2% worse speed

; This depacker is doing decoding of a single data block with size <=16KB
; No SHAFF header is expected - it's already known that it's SHAFF0 format:
; 1st byte sets a Key to use further instead of #FF (but usually it's #FF)
; then any byte other than Key goes directly to output, but for Key see below
; #FF 00000000 - single byte #FF, otherwise
; #FF 0xxxxxxx LENGTH - distance 1..127 and LENGTH (see below)
; #FF 10xxxxxx LENGTH - distance 128..190 and LENGTH (see below), but
; #FF 10111111 LENGTH - reuses previous long distance -191 or longer
; #FF 11xxxxxx xxxxxxxx LENGTH - directly encoded long negative distance, but
; #FF 11000000 00000000 - end of block (with no length after)
; where LENGTH encoded by 1 or 2 bytes:
; 1xxxxxxx - for 4..131
; 01xxxxxx - for 132..195
; 00xxxxxx xxxxxxxx - direcly encoded length for up to 16383

; Input:
;   HL - address of packed data that starts with a key byte (usually #FF)
;   DE - pointer to the buffer where depacked data should appear (up to 16KB)
; Output:
;   HL - address of the next byte after packed data
;   DE - address of the next byte after unpacked data
;   flag C - indicates an error (in this version it's always 0)

DESHAFF0:
if !TURBO
	ld	b,(hl) ; B is a Key byte (#FF in normal case)
else
	ld	a,(hl) ; Store a Key to the code in 2 places
	ld	(DSH0L+2),a
	ld	(DSH0LG+1),a
endif
	inc	hl
DSH0L:
if !TURBO
	ld	a,(hl)   ; RELO +7=7  ; main loop
	inc	hl       ; RELO +6=13
	cp	b        ; RELO +4=17 ; compare with a Key
	jr	z,DSH0FF ; RELO +7=24 (12 if jump)
DSH0LL:	ld	(de),a   ; RELO +7=31 ; it was not a Key so decode byte as is
	inc	de       ; RELO +6=37
	jr	DSH0L    ; RELO +12=49 (copying of unpacked data 2.33*LDIR)
else
	ld	bc,-1    ; TURBO 10 (before)
MLDI	macro
	ld	a,b      ;+4=4
	cp	(hl)     ;+7=11
	jr	z,DSH0FF ;+7=18
	ldi              ;+16=34
endm ; end of macro MLDI
	MLDI ; 1
	MLDI ; 2
	MLDI ; 3
	MLDI ; 4
	MLDI ; 5
	MLDI ; 6
	MLDI ; 7
	MLDI ; 8
	MLDI ; 9
	MLDI ; 10
	MLDI ; 11
	MLDI ; 12
	MLDI ; 13
	MLDI ; 14
	MLDI ; 15
	MLDI ; 16
	MLDI ; 17
	MLDI ; 18
	MLDI ; 19
	MLDI ; 20
	MLDI ; 21
	jp	DSH0L    ; TURBO 10 (after)
	; so it's 10+21*34+10=734 and 734/21=34.95t/byte (1.66*LDIR)
endif
DSH0FF:
if TURBO
	inc	hl ; TURBO code requires HL to be incremented here
endif
	ld	a,(hl) ; read byte after Key
	inc	hl
	or	a ; check for 0
	jr	nz,DSH0F1
if !TURBO
	ld	a,b ; it was 0 after a Key so decode it as Key value
	jr	DSH0LL ; go back to the loop
else
DSH0LG:	ld	a,#FF ; it was 0 after a Key so decode it as Key value
	ld	(de),a ; store Key itself
	inc	de ; and increment destination pointer
	jp	DSH0L ; then go back to the beginning of the loop
endif
DSH0F1:
	ld	c,a ; it was not 0 after a Key so store it and
	and	#C0 ; check if it's 1-byte distance
	cp	#C0 ; by comparing 2 most significant bits with 11
	jr	z,DSH0F2
	ld	a,c ; now check if distance is 191 (special case)
	cp	#BF
	jr	z,DSH0F3 ; go to special case handler
if !TURBO
	push	bc ; temporarily push Key to stack
endif
	xor	a ; clear A and flagC
	ld	b,a ; here BC is a distance back (1..190)
	push	hl
	ld	h,a ; clear H
	ld	l,a ; clear L
	sbc	hl,bc ; calculate negative distance HL = 0 - BC
	ld	b,h
	ld	c,l ; now BC is a negative distance for 1-byte case
	pop	hl
if !TURBO
	jr	DSH0LN ; go to read length
else
	jp	DSH0LN ; go to read length faster
endif
DSH0F2:
if !TURBO
	push	bc ; temporary push Key to stack
endif
	ld	b,c ; it should be 2-byte distance, so this is higher byte
	ld	c,(hl) ; read lower byte of the distance
	inc	hl
	push	bc ; now BC is a negative distance for 2-byte case
	pop	iy ; store negative long distance to IY for future use
	; check for end of the block
	ld	a,b
	cp	#C0 ; compare higher byte with #C0
	jr	nz,DSH0LN ; go to read length
	ld	a,c
	or	a ; compare lower byte with #00
	jr	nz,DSH0LN ; go to read length
if !TURBO
	pop	bc ; balance the stack before return
endif
	ret
DSH0F3:
if !TURBO
	push	bc ; temporary push Key to stack
endif
	push	iy ; retrieve stored distance
	pop	bc ; now BC is a negative distance for special case
DSH0LN:	ld	a,(hl) ; read length
	inc	hl
	bit	7,a
if !TURBO
	jr	z,DSH0L1
else
if TURBO2
	jp	z,DSH0L1
else
	jr	z,DSH0L1
endif
endif
	; 1xxxxxxx - Length 4..131 (0x80->4, 0x81->5 ... 0xFF->131)
	sub	#7C
DSH0L0:	push	de ; push DE temporarily
	ld	d,0 ; higher byte of the length is 0
	ld	e,a ; lower byte of the length is taken from A
if !TURBO
	jr	DSH0L3
else
if TURBO2
	cp	12
	jp	c,DSH0L3
DSH0L3N:
	ex	(sp),hl ; exchange top of the stack (stored DE) with HL
	push	de ; push length of the data
	ld	d,h
	ld	e,l ; now DE is current destination address
	add	hl,bc ; now HL is an address of the reference
	pop	bc ; now BC is length of the data to copy
	xor a ; turbo LDIR starts here
	sub c
	and #3F
	add a,a
	ld (DSH0L3N1+1),a
DSH0L3N1:
	jr nz,DSH0L3N2
DSH0L3N2:
LDI8	macro
	ldi ; +16
	ldi ; +16
	ldi ; +16
	ldi ; +16
	ldi ; +16
	ldi ; +16
	ldi ; +16
	ldi ; +16
endm ; end of macro LDI8
	LDI8 ; 1
	LDI8 ; 2
	LDI8 ; 3
	LDI8 ; 4
	LDI8 ; 5
	LDI8 ; 6
	LDI8 ; 7
	LDI8 ; 8
	jp	pe,DSH0L3N2
	; turbo LDIR ends here
	pop	hl ; restore old source
	jp	DSH0L ; go back to main loop
else
	jp	DSH0L3
endif
endif
DSH0L1: bit	6,a
	jr	z,DSH0L2
	; 01xxxxxx - Length 132..195 (0x40->132, 0x41->133 ... 0x7F->195)
	add	a,#44
if !TURBO
	jr	DSH0L0
else
	push	de ; push DE temporarily
	ld	d,0 ; higher byte of the length is 0
	ld	e,a ; lower byte of the length is taken from A
if TURBO2
	jp	DSH0L3N
else
	jp	DSH0L3
endif
endif
DSH0L2:	push	de ; push DE temporarily
	; 00xxxxxx xxxxxxxx - Length up to 16383
	ld	d,a ; higher byte of the length
	ld	e,(hl) ; read lower byte of the length
	inc	hl
if TURBO
if TURBO2
	jp	DSH0L3N ; go to faster version in case of non-relocatable code
endif
endif
DSH0L3:	ex	(sp),hl ; exchange top of the stack (stored DE) with HL
	push	de ; push length of the data
	ld	d,h
	ld	e,l ; now DE is current destination address
	add	hl,bc ; now HL is an address of the reference
	pop	bc ; now BC is length of the data to copy
	ldir ; perform copying of the data
	pop	hl ; restore old source
if !TURBO
	pop	bc ; restore a Key
	jr	DSH0L ; go back to main loop
else
	jp	DSH0L ; go back to main loop faster
endif
