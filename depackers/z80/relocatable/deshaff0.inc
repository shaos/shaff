; Size of this relocatable code is 104 bytes
; HL - address of packed data (data starts with a key byte)
; DE - pointer to the buffer where depacked data should appear (up to 16KB)

DESHAFF0:
	db #46,#23,#7e,#23,#b8,#28,#04,#12,#13,#18,#f7,#7e,#23,#b7,#20,#03
	db #78,#18,#f4,#4f,#e6,#c0,#fe,#c0,#28,#12,#79,#fe,#bf,#28,#1f,#c5
	db #af,#47,#e5,#67,#6f,#ed,#42,#44,#4d,#e1,#18,#16,#c5,#41,#4e,#23
	db #c5,#fd,#e1,#78,#fe,#c0,#20,#0a,#79,#b7,#20,#06,#c1,#c9,#c5,#fd
	db #e5,#c1,#7e,#23,#cb,#7f,#28,#08,#d6,#7c,#d5,#16,#00,#5f,#18,#0c
	db #cb,#77,#28,#04,#c6,#44,#18,#f2,#d5,#57,#5e,#23,#e3,#d5,#54,#5d
	db #09,#c1,#ed,#b0,#e1,#c1,#18,#9a
