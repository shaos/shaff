; SHAFF0 block depacker for Z80/Z180 written by Shaos on 28-MAR-2021
; See http://nedoPC.org/forum/ for more info about SHAFF packer
; This code is PUBLIC DOMAIN - use it on your own RISK!

; This depacker is doing decoding of a single data block with size <=16KB
; No SHAFF header is expected - it's already known that it's SHAFF0 format:
; 1st byte sets a Key to use further instead of #FF (but usually it's #FF)
; then any byte other than Key goes directly to output
; #FF 00000000 - single byte #FF, otherwise
; #FF 0xxxxxxx LENGTH - distance 1..127 and LENGTH (see below)
; #FF 10xxxxxx LENGTH - distance 128..190 and LENGTH (see below), but
; #FF 10111111 LENGTH - reuses previous long distance -191 or longer
; #FF 11xxxxxx xxxxxxxx LENGTH - directly encoded long negative distance, but
; #FF 11000000 00000000 - end of block (with no length after)
; where LENGTH encoded by 1 or 2 bytes:
; 1xxxxxxx - for 4..131
; 01xxxxxx - for 132..195
; 00xxxxxx xxxxxxxx - direcly encoded length for up to 16383

; Size of this relocatable code is 104 bytes
; HL - address of packed data (data starts with a key byte)
; DE - pointer to the buffer where depacked data should appear (up to 16KB)

DESHAFF0:
	ld	b,(hl) ; B is a Key byte (#FF in normal case)
	inc	hl
DSH0L:	ld	a,(hl)   ; +7=7  ; main loop
	inc	hl       ; +6=13
	cp	b        ; +4=17 ; compare with a Key
	jr	z,DSH0FF ; +7=24 (the best case goes through)
DSH0LL:	ld	(de),a   ; +7=31 ; it was not a Key so decode byte as is
	inc	de       ; +6=37
	jr	DSH0L    ; +12=49 (copying of unpacked data 2.33*LDIR)
DSH0FF:	ld	a,(hl) ; read byte after Key
	inc	hl
	or	a ; check for 0
	jr	nz,DSH0F1
	ld	a,b ; it was 0 after a Key so decode it as Key value
	jr	DSH0LL ; go back to the loop
DSH0F1:	ld	c,a ; it was not 0 after a Key so store it and
	and	#C0 ; check if it's 1-byte distance
	cp	#C0 ; by comparing 2 most significant bits with 11
	jr	z,DSH0F2
	ld	a,c ; now check if distance is 191 (special case)
	cp	#BF
	jr	z,DSH0F3 ; go to special case handler
	push	bc ; temporarily push Key to stack
	xor	a ; clear A and flagC
	ld	b,a ; here BC is a distance back (1..190)
	push	hl
	ld	h,a ; clear H
	ld	l,a ; clear L
	sbc	hl,bc ; calculate negative distance HL = 0 - BC
	ld	b,h
	ld	c,l ; now BC is a negative distance for 1-byte case
	pop	hl
        jr	DSH0LN ; go to read length
DSH0F2:	push	bc ; temporary push Key to stack
	ld	b,c ; it should be 2-byte distance, so this is higher byte
        ld	c,(hl) ; read lower byte of the distance
	inc	hl
	push	bc ; now BC is a negative distance for 2-byte case
	pop	iy ; store negative long distance to IY for future use
	; check for end of the block
	ld	a,b
	cp	#C0 ; compare higher byte with #C0
	jr	nz,DSH0LN ; go to read length
	ld	a,c
	or	a ; compare lower byte with #00
	jr	nz,DSH0LN ; go to read length  
	pop	bc ; balance the stack before return
	ret
DSH0F3:	push	bc ; temporary push Key to stack
	push	iy ; retrieve stored distance
	pop	bc ; now BC is a negative distance for special case
DSH0LN:	ld	a,(hl) ; read length
	inc	hl
	bit	7,a
	jr	z,DSH0L1
	; 1xxxxxxx - Length 4..131 (0x80->4, 0x81->5 ... 0xFF->131)
        sub	#7C
DSH0L0:	push	de ; push DE temporarily
	ld	d,0 ; higher byte of the length is 0
	ld	e,a ; lower byte of the length is taken from A
	jr	DSH0L3
DSH0L1: bit	6,a
	jr	z,DSH0L2
	; 01xxxxxx - Length 132..195 (0x40->132, 0x41->133 ... 0x7F->195)
	add	a,#44
	jr	DSH0L0
DSH0L2:	push	de ; push DE temporarily
	; 00xxxxxx xxxxxxxx - Length up to 16383
	ld	d,a ; higher byte of the length
	ld	e,(hl) ; read lower byte of the length
	inc	hl
DSH0L3:	ex	(sp),hl ; exchange top of the stack (stored DE) with HL
	push	de ; push length of the data
	ld	d,h
	ld	e,l ; now DE is current destination address
	add	hl,bc ; now HL is an address of the reference
	pop	bc ; now BC is length of the data to copy
	ldir ; perform copying of the data
	pop	hl ; restore old source
	pop	bc ; restore a Key
	jr	DSH0L ; go back to main loop


