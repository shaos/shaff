# SHAFF

**SHAFF** is [LZSS](https://en.wikipedia.org/wiki/Lempel-Ziv-Storer-Szymanski)-like
lossless data compression format that I started designing in 2013. It targets retro computers
and small devices providing public domain depackers written in Assembler of multiple target platform
(still working on broad coverage) and "copy-lefted" packer/depacker for PC represented by a single C-file
![shaff.c](https://gitlab.com/shaos/shaff/raw/master/shaff.c)

See ![SHAFF format](https://gitlab.com/shaos/shaff/raw/master/SHAFF) (theoretical size limit is 1GB)

**Format SHAFF0 - FULLY IMPLEMENTED (Feb 8, 2017)**

**Format SHAFF1 - FULLY IMPLEMENTED (Feb 8, 2017)**

**Format SHAFF2 - EXPERIMENTAL SUPPORT (Feb 12, 2017)**

~~~~
SHAFF v1.2 (C) 2013,2017 A.A.Shabarshin <me@shaos.net>


Usage:
    shaff [options] filename

Encoding options:
    -0 to use SHAFF0 file format (by default)
    -1 to use SHAFF1 file format
    -2 to use SHAFF2 file format (experimental)
    -b to compress blocks into separate files
    -bN to compress only block N
    -bN-M to compress blocks N..M
    -lN to limit length of matches (default value is 4 for SHAFF0 and 2 for SHAFF1/2)
    -xHH to set prefix byte other than FF (applicable only to SHAFF0 file format)
    -e to set default table for English text (applicable only to SHAFF2 file format)

Decoding options:
    -d to decode compressed SHAFF file to file
    -c to decode compressed SHAFF file to screen
~~~~

SHAFF packer/depacker command line utility was tested on:

* GCC v4.3.2 in 32-bit Debian Linux on PowerPC G4 (big endian)
* Borland C++ 5.5.1 in 32-bit Windows XP on Intel Core Duo (little endian)
* GCC v6.3.0 in 64-bit Debian Linux on AMD A10 (little endian)

Currently available depackers:

* plain C as part of shaff.c (shaff -d filenameFF)
* Z80/Z180 depackers for SHAFF0 and SHAFF1 separate blocks (without header)

Presented coding schemes and algorithms were developed from scratch slnce 2013
in my spare time on my home computers entirely by me and to the best of my
knowledge and belief it does not violate or infringe any existing patent or
trade secret and it's unrelated in any way to my work for any of my former
or present employers or clients.

Alexander "Shaos" Shabarshin, May 2021
